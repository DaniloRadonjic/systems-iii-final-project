let button_edit = document.getElementsByClassName('button_edit');
let button_delete = document.getElementsByClassName('button_delete');
let input_array = document.getElementsByTagName('input');
let option_array = document.getElementsByTagName('select');
let user_data = document.getElementsByClassName('edit');
let visible = false;

function show_form(event) {
	event.preventDefault();
	if (visible === false) {
		for (let i = 0; i < input_array.length; i++) {
			input_array[i].style.display = 'inline';
		}
		for (let i = 0; i < option_array.length; i++) {
			option_array[i].style.display = 'inline';
		}
		for (let i = 0; i < user_data.length; i++) {
			user_data[i].style.display = 'none';
		}
		for (let i = 0; i < button_edit.length; i++) {
			button_edit[i].innerHTML = 'Discard';
			button_delete[i].style.display = 'none';
		}
		visible = true;
	} else {
		for (let i = 0; i < input_array.length; i++) {
			input_array[i].style.display = 'none';
		}
		for (let i = 0; i < option_array.length; i++) {
			option_array[i].style.display = 'none';
		}
		for (let i = 0; i < user_data.length; i++) {
			user_data[i].style.display = 'inline';
		}
		for (let i = 0; i < button_edit.length; i++) {
			button_edit[i].innerHTML = 'Edit profile';
			button_delete[i].style.display = 'inline';
		}
		visible = false;
	}
}

for (let i = 0; i < button_edit.length; i++) {
	button_edit[i].addEventListener('click', show_form);
}

function confirm_action() {
	let message = prompt("Please, type in 'CONFIRM' and press the OK button to complete the action");
	return message === "CONFIRM";
}