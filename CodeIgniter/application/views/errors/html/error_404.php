<!DOCTYPE html>
<html>
<head>
	<title>404 page</title>
	<style>
		@import url('https://fonts.googleapis.com/css?family=Montserrat');
		@import url('https://fonts.googleapis.com/csss?family=Open+Sans');

		html, body {
			height: 100%;
			margin: 0;
		}

		.container {
			display: flex;
			flex-wrap: wrap;
			flex-direction: row;
			height: 100%;
		}

		.left {
			flex: 60%;
			height: 100%;
		}

		.left h1 {
			font-family: Montserrat, sans-serif;
			font-size: 64px;
			color: #41436A;
			margin: 220px 0px 20px 200px;
		}

		.left p {
			width: 60%;
			font-size: 20px;
			font-family: 'Open Sans', sans-serif;
			margin: 60px 220px;
		}

		.left a {
			color: #41436A;
		}

		.right {
			flex: 40%;
			height: 100%;
			text-align: left;
		}

		.right img {
			width: 420px;
			height: 245px;
			margin: 240px 0px;
		}
	</style>
</head>
<body>

	<div class = 'container'>
		<div class = 'left'>
			<h1>Oops!</h1>
			<p>We can't seem to find the page you are looking for.<br />Make sure the URL has no misspellings.</p>
			<p><a href = "<?php echo base_url('landing_page'); ?>">Back to home page</a></p>
		</div>
		<div class = 'right'>
			<img src = "<?php echo base_url('images/404_icon.png'); ?>" alt = '404 icon' />
		</div>
	</div>

</body>
</html>