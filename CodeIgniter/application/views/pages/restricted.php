<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<style>
		* {
			box-sizing: border-box;
			font-family: 'Open Sans', sans-serif;
			text-align: center;
		}

		h1 {
			font-family: Montserrat, sans-serif;
			color: #41436A;
			margin-top: 100px;
			font-size: 36px;
		}

		a {
			color: #41436A;
		}
	</style>
</head>
<body>

	<h1>Restricted Access</h1>
	<p>The page you are trying to access is available only to signed in users.</p>
	<a href = "<?php echo base_url('landing_page'); ?>"><p>Go back</p></a>

</body>
</html>
