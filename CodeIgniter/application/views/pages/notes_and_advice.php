<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<style>
		* {
			box-sizing: border-box;
			font-family: 'Open Sans', sans-serif;
		}

		h1 {
			font-family: Montserrat, sans-serif;
			color: #41436A;
			padding: 20px;
			font-size: 36px;
		}

		dl {
			margin: 20px 280px;
			padding: 20px;
		}

		dt {
			color: #41436A;
			font-size: 1.50em;
			font-weight: bold;
			padding: 16px;
		}

		dd {
			margin-bottom: 16px;
		}

	</style>
</head>
<body>

	<h1>Notes & Advice</h1>
	<dl>
		<dt>Right information</dt>
		<dd>Letting customers know you have food at lower price is beneficial, but to ensure a better experience (and increase chance of selling food), provide clear and right information.</dd><br />
		<dt>Fair price</dt>
		<dd>Rmemember - This is food which is not as fresh as the day it has been made. It is advisable to price your product <b>30</b> to <b>50%</b> of the ordinary price. (i.e. if ordinary price of your product is 2€, the new price should be around 1€).<br /> If you price your product too much, customers may not be interested. And that's not what we want.</dd><br />
		<dt>More for less</dt>
		<dd>An important thing to notice is that you are offering an instance of the product. (and not the product itself!)<br />Often, customers prefer buying more instances of the product. You have a possibility to offer them more instances at once. To do so, set the quantity (number of instances) and adjust the price. (i.e. instead of creating each post for one piece of cake, try to offer two pieces, in only one post.) </dd><br />
		<dt>Limited time for post</dt>
		<dd>Don't forget - this website is intended to let customers know you have surplus food to sell. It should not be used as main channel of selling food.The food you offer here is most likely to expire in couple of days. (or less)<br />Therefore, every post you create is visible to customers for the next 24 hours.</dd><br />
		<dt>When to pick?</dt>
		<dd>If you would like to specify the time for customers to come to your store and purchase the product, you can do so by choosing pick-up time. (hours and minutes)</dd><br />
		<dt>Dealing with reservations</dt>
		<dd>Your product has been reserved? Awesome! Make sure that the product is packed and ready by the time customer comes to your store. A customer will show you a unique product code (6-digit code), which you should insert in the special field. (on a webpage 'My posts')<br />If given code matches with the product, you can rest assured that the right customer has come. All you need to do left is sell the product to the customer. System will process your interaction and issue an invoice. (accessible through a webpage 'My invoices')</dd><br />
	</dl>
	
</body>
</html>