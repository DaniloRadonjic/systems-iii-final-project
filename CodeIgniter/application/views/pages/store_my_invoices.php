<?php 
if (!isset($this->session->userdata['s_signed_in'])) {
	redirect('restricted');
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : "My Invoices"; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/posts_list.css' />
</head>
<body>
	
	<div class = 'navigation_bar'>
		<ul>
			<a href = "<?php echo site_url('store_my_profile'); ?>"><li>My profile</li></a>
			<a href = "<?php echo site_url('store_create_post'); ?>"><li>Create post</li></a>
			<a href = "<?php echo base_url('Store/displayposts'); ?>"><li>My posts</li></a>
			<li class = 'active'>My invoices</li>
			<a href = "<?php echo base_url('Store/signout'); ?>"><li>Sign out</li></a>
		</ul>		
	</div>
	
	<div class = 'heading'>
		<h1>My invoices</h1>
	</div>
	
	<div>
		<p class = 'success'><?php echo $this->session->flashdata('success'); ?></p>
		<p class = 'error'><?php echo $this->session->flashdata('error'); ?></p>
	</div>

	<div class = 'main'>
		<table class = 'smaller_table'>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Issued at</th>
			</tr>
			<tr class = 'empty_row'></tr>
			<?php if ($invoices->num_rows() === 0) { ?>
				<tr>
					<td colspan = '5'>You have no invoices yet.</td>
				</tr>
			<?php } ?>
			<?php $i = 1; foreach($invoices->result('array') as $row) { ?>
				<tr>
					<td><?php echo $i < 10 ? 0 . $i : $i; ?></td>
					<td class = 'product_name'><?php echo $row['p_name']; ?></td>
					<td><?php echo $row['p_quantity']; echo $row['p_quantity'] == 1 ? ' piece' : ' pieces'; ?></td>
					<td>€<?php echo $row['p_price'] ?></td>
					<td><?php echo date('d F, Y (H:i)', strtotime($row['i_issued_at'])); ?></td>
				</tr>
				<tr class = 'empty_row'></tr>
				<?php $i++; } ?>
			</table>
		</div>

	</body>
	</html>