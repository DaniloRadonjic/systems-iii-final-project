<?php 
if (!isset($this->session->userdata['s_signed_in'])) {
	redirect('restricted');
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : "My Posts"; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/posts_list.css' />
</head>
<body>

	<div class = 'navigation_bar'>
		<ul>
			<a href = "<?php echo site_url('store_my_profile'); ?>"><li>My profile</li></a>
			<a href = "<?php echo site_url('store_create_post'); ?>"><li>Create post</li></a>
			<li class = 'active'>My posts</li>
			<a href = "<?php echo base_url('Store/displayinvoices'); ?>"><li>My invoices</li></a>
			<a href = "<?php echo site_url('Store/signout'); ?>"><li>Sign out</li></a>
		</ul>		
	</div>
	
	<div class = 'heading'>
		<h1>My posts</h1>
	</div>
	
	<div >
		<p class = 'success'><?php echo $this->session->flashdata('success'); ?></p>
		<p class = 'error'><?php echo $this->session->flashdata('error'); ?></p>
	</div>

	<div class = 'main'>
		<table>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Pick-up time</th>
				<th>Status</th>
				<th>Code</th>
				<th></th>
			</tr>
			<tr class = 'empty_row'></tr>
			<?php if ($products->num_rows() === 0) { ?>
				<tr>
					<td colspan = '8'>You have no active posts yet.<br /><a href = "<?php echo site_url('store_create_post'); ?>">Create new post</a></td>
				</tr>
			<?php } ?>
			<?php $i = 1; foreach($products->result() as $row) { ?>
				<tr>
					<td><?php echo $i < 10 ? 0 . $i : $i; ?></td>
					<td class = 'product_name'><?php echo $row->name; ?></td>
					<td><?php echo $row->quantity; echo $row->quantity == 1 ? ' piece' : ' pieces'; ?></td>
					<td>€<?php echo $row->price; ?></td>
					<td><?php echo $row->pick_until; ?>h</td>
					<td><?php echo $row->is_reserved == '0' ? "<span class = 's_active'>Active</span>" : "<span class = 's_reserved'>Reserved</span>"; ?></td>
					<form method = 'POST' action = "<?php echo base_url('Store/checkcode'); ?>">
						<input type = 'hidden' name = 'p_id' value = "<?php echo $row->id; ?>" />
						<td><input type = 'text' name = 'code' oninput = "this.value = this.value.replace(/[^0-9]/, '').replace(/(\..*)\./);" minlength = '6' maxlength = '6' placeholder = 'insert' required /></td>
						<td><input type = 'submit' name = 'check' value = 'Check' /></td>
					</form>
				</tr>
				<tr class = 'empty_row'></tr>
				<?php $i++; } ?>
			</table>
		</div>

	</body>
	</html>

