<?php 
if (!isset($this->session->userdata['s_signed_in'])) {
	redirect('restricted');
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/create_post.css' />
</head>
<body>

	<div class = 'navigation_bar'>
		<ul>
			<a href = "<?php echo site_url('store_my_profile'); ?>"><li>My profile</li></a>
			<li class = 'active'>Create post</li>
			<a href = "<?php echo base_url('Store/displayposts'); ?>"><li>My posts</li></a>
			<a href = "<?php echo base_url('Store/displayinvoices'); ?>"><li>My invoices</li></a>
			<a href = "<?php echo base_url('Store/signout'); ?>"><li>Sign out</li></a>
		</ul>		
	</div>
	
	<div class = 'heading'>
		<h1>Create post</h1>
	</div>
	
	<div class = 'main'>
		<form method = 'POST' action = "<?php echo base_url('Store/publishpost'); ?>">
			<label for = 'name'>Name of product</label>
			<input id = 'name' type = 'text' name = 'name' placeholder = 'e.g. Sacher cake' required />
			<br />
			<label class = 'smaller_font' for = 'quantity'>Quantity</label>
			<input id = 'quantity' type = 'number' name = 'quantity' min = '1' max = '20' step = '1' placeholder = 'e.g. 2 (pcs)' required />
			<label>Price (total)</label>
			<label class = 'smaller_font' for = 'euro'>Euros</label>
			<input id = 'euro' type = 'number' name = 'euro' min = '0' max = '99' step = '1' pattern = "[0-9]*" placeholder = 'e.g. 1' required />
			<br />
			<label class = 'smaller_font' for = 'cent'>Cents</label>
			<input id = 'cent' type = 'number' name = 'cent' min = '0' max = '99' step = '1' pattern = "[0-9]*" placeholder = 'e.g. 25' required />
			<label>Pick-up time</label>
			<label class = 'smaller_font' for = 'hour'>Hours</label>
			<input id = 'hour' type = 'number' name = 'hour' min = '0' max = '23' step = '1' pattern = "[0-9]*" placeholder = 'e.g. 19' required />
			<br />
			<label class = 'smaller_font' for = 'minute'>Minut</label>
			<input id = 'minute' type = 'number' name = 'minute' min = '0' max = '59' step = '1' pattern = "[0-9]*" placeholder = 'e.g. 00' required />
			<input type = 'submit' name = 'create' value = 'Create' />
			<p><span>Note: </span>Posts are visible for 24 hours!<br /><a href = 'notes_and_advice' target = '_blank'>Find out more</a></p>
		</form>
	</div>

</body>
</html>