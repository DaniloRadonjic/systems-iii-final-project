<!DOCTYPE html>
<html>
<head>
	<title>Maintenance Break</title>
	<style>
		* {
			box-sizing: border-box;
			font-family: 'Open Sans', sans-serif;
			text-align: center;
		}

		h1 {
			font-family: Montserrat, sans-serif;
			color: #41436A;
			margin-top: 100px;
			font-size: 36px;
		}

		a {
			color: #41436A;
		}

		img {
			margin-top: 40px;
			text-align: center;
			height: 235px;
			width: 185px;
		}

	</style>
</head>
<body>

	<h1>We will be back soon!</h1>
	<p>We are doing some maintenance and server upkeep at the moment, and will be back shortly.<br />Thank you for being patient.</p>
	<img src = "<?php echo base_url('images/maintenance_icon.png'); ?>" alt = 'maintenance icon' />
</body>
</html>
