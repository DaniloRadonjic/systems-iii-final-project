<?php 
if (!isset($this->session->userdata['c_signed_in'])) {
	redirect('restricted');
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : "My reservations"; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/posts_list.css' />
</head>
<body>
	
	<div class = 'navigation_bar'>
		<ul>
			<a href = "<?php echo site_url('customer_my_profile'); ?>"><li>My profile</li></a>
			<a href = "<?php echo base_url('Customer/displayposts'); ?>"><li>Find food</li></a>
			<li class = 'active'>My reservations</li>
			<a href = "<?php echo base_url('Customer/displayinvoices'); ?>"><li>My invoices</li></a>
			<a href = "<?php echo base_url('Customer/signout'); ?>"><li>Sign out</li></a>
		</ul>		
	</div>
	
	<div class = 'heading'>
		<h1>My reservations</h1>
	</div>
	
	<div>
		<p class = 'success'><?php echo $this->session->flashdata('success'); ?></p>
		<p class = 'error'><?php echo $this->session->flashdata('error'); ?></p>
	</div>

	<div class = 'main'>
		<table>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Pick-up time</th>
				<th>Offered by</th>
				<th>Address</th>
				<th>Reserved at</th>
				<th>Code</th>
			</tr>
			<tr class = 'empty_row'></tr>
			<?php if ($reservations->num_rows() === 0) { ?>
				<tr>
					<td colspan = '9'>You have no reservations yet.</td>
				</tr>
			<?php } ?>
			<?php $i = 1; foreach($reservations->result('array') as $row) { ?>
				<tr>
					<td><?php echo $i < 10 ? 0 . $i : $i; ?></td>
					<td class = 'product_name'><?php echo $row['p_name']; ?></td>
					<td><?php echo $row['p_quantity']; echo $row['p_quantity'] == 1 ? ' piece' : ' pieces'; ?></td>
					<td>€<?php echo $row['p_price'] ?></td>
					<td><?php echo $row['p_pick_until']; ?>h</td>
					<td><?php echo $row['s_name']; ?></td>
					<td><?php echo $row['s_address']; ?></td>
					<td><?php echo date('d F, Y (H:i)', strtotime($row['r_reserved_at'])); ?></td>
					<td><?php echo $row['p_code']; ?></td>
				</tr>
				<tr class = 'empty_row'></tr>
				<?php $i++; } ?>
			</table>
		</div>

	</body>
	</html>