<?php 
if (isset($this->session->userdata['s_signed_in'])) redirect('store_my_profile');
if (isset($this->session->userdata['c_signed_in'])) redirect('customer_my_profile');
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/landing_page.css' />
</head>
<body>

	<div class = 'container'>
		<div class = 'left'>
			<h1>Make an impact on <br />reducing food waste</h1>
			<br />
			<p>A system dedicated to connecting individuals who seek for delicious, low-cost meals, with local stores that have yet fresh unsold products.
			</div>
			<div class = 'right'>
				<h1>Sign in</h1>
				<form method = 'POST' action = "<?php echo base_url('User/signin'); ?>">
					<label for = 'email'>Email address</label>
					<input type = 'text' name = 'email' placeholder = 'type here ...' required />
					<label for = 'password'>Password</label>
					<input type = 'password' name = 'password' maxlength = '20' placeholder = 'type here ...' required />
					<input type = 'submit' name = 'sign_in' value = 'Sign in' />
				</form>
				<p class = 'success'><?php echo $this->session->flashdata('success'); ?></p>
				<p class = 'incorrect'><?php echo $this->session->flashdata('incorrect'); ?></p>
				<div class = 'sign_up_links'>
					<p>Don't have an account yet?</p>
					<ul>
						<li><a href = 'customer_sign_up'>Sign up as customer</a></li>
						<li><a href = 'store_sign_up'>Sign up as store</a></li>
					</ul>
				</div>
			</div>
			
		</body>
		</html>