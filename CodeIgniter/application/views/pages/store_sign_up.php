<?php 
if (isset($this->session->userdata['s_signed_in'])) redirect('store_my_profile');
if (isset($this->session->userdata['c_signed_in'])) redirect('customer_my_profile');
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/sign_up.css' />
</head>
<body>

	<div class = 'container'>
		<div class = 'left'>
			<h1>Sign up <sup>as store</sup></h1>
			<form method = 'POST' action = "<?php echo base_url('Store/savedata'); ?>">
				<label for = 'email'>Email address</label>
				<input type = 'email' name = 'email' placeholder = 'type here ...' required />
				<label for = 'password'>Password</label>
				<input type = 'password' name = 'password' maxlength = '20' placeholder = 'type here ...' required />
				<label for = 'name'>Name of store</label>
				<input type = 'text' name = 'name' placeholder = 'type here ...' required />
				<label for = 'city'>City</label>
				<select name = 'city' required>
					<option value = '' disabled selected>select</option>
					<option value = 'Ljubljana'>Ljubljana</option>
					<option value = 'Maribor'>Maribor</option>
					<option value = 'Celje'>Celje</option>
					<option value = 'Kranj'>Kranj</option>
					<option value = 'Koper'>Koper</option>
				</select>
				<label for = 'address'>Address</label>
				<input type = 'text' name = 'address' placeholder = 'type here ...' required />
				<label for = 'terms'><input type = 'checkbox' name = 'terms' required />I agree to <a href = 'terms_and_conditions' target = '_blank'>terms & conditions</a></label>
				<input type = 'submit' name = 'sign_up' value = 'Sign up' />
			</form>
			<p class = 'error'><?php echo $this->session->flashdata('error'); ?></p>
			<p>Already have an account? <a href = 'landing_page'>Sign in</a></p>
		</div>
		<div class = 'right'>
			<h1>Secure account</h1>
			<p>It is considered a good practice that your password has:</p>
			<ul>
				<li>length of 8 to 20 characters</li>
				<li>at least one uppercase letter</li>
				<li>couple of special characters</li>
			</ul>
		</div>
	</div>

</body>
</html>