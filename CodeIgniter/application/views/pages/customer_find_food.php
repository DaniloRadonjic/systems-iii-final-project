<?php 
if (!isset($this->session->userdata['c_signed_in'])) {
	redirect('restricted');
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : "Find Food"; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/posts_list.css' />
</head>
<body>

	<div class = 'navigation_bar'>
		<ul>
			<a href = "<?php echo site_url('customer_my_profile'); ?>"><li>My profile</li></a>
			<li class = 'active'>Find food</li>
			<a href = "<?php echo base_url('Customer/displayreservations'); ?>"><li>My reservations</li></a>
			<a href = "<?php echo base_url('Customer/displayinvoices'); ?>"><li>My invoices</li></a>
			<a href = "<?php echo base_url('Customer/signout'); ?>"><li>Sign out</li></a>
		</ul>		
	</div>
	
	<div class = 'heading'>
		<h1>Find food</h1>
	</div>
	
	<div>
		<p class = 'success'><?php echo $this->session->flashdata('success'); ?></p>
		<p class = 'error'><?php echo $this->session->flashdata('error'); ?></p>
		<p class = 'posts_city'>Showing posts for: <span><?php $city = $this->session->flashdata('city'); echo isset($city) ? $city : $this->session->userdata['c_signed_in']['city']; ?></span></p>
	</div>

	<div class = 'filter_posts'>
		<form method = 'GET' action = "<?php echo base_url('Customer/filterposts'); ?>">
			<select name = 'city' required>
				<option value = '' disabled selected>choose</option>
				<option value = 'Ljubljana'>Ljubljana</option>
				<option value = 'Maribor'>Maribor</option>
				<option value = 'Celje'>Celje</option>
				<option value = 'Kranj'>Kranj</option>
				<option value = 'Koper'>Koper</option>
			</select>
			<input type = 'submit' name = 'filter' value = 'Filter' />
		</form>
	</div>

	<div class = 'main'>
		<table>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Pick-up time</th>
				<th>Offered by</th>
				<th>Address</th>
				<th></th>
			</tr>
			<tr class = 'empty_row'></tr>
			<?php if ($products->num_rows() === 0) { ?>
				<tr>
					<td colspan = '8'>There is no surplus food yet.</td>
				</tr>
			<?php } ?>
			<?php $i = 1; foreach($products->result('array') as $row) { ?>
				<tr>
					<td><?php echo $i < 10 ? 0 . $i : $i; ?></td>
					<td class = 'product_name']><?php echo $row['p_name']; ?></td>
					<td><?php echo $row['p_quantity']; echo $row['p_quantity'] == 1 ? ' piece' : ' pieces'; ?></td>
					<td>€<?php echo $row['p_price'] ?></td>
					<td><?php echo $row['p_pick_until']; ?>h</td>
					<td><?php echo $row['s_name']; ?></td>
					<td><?php echo $row['s_address']; ?></td>
					<form method = 'POST' action = "<?php echo base_url('Customer/reservepost'); ?>">
						<input type = 'hidden' name = 'p_id' value = "<?php echo $row['p_id']; ?>" />
						<td><input type = 'submit' name = 'reserve' value = 'Reserve' /></td>
					</form>
				</tr>
				<tr class = 'empty_row'></tr>
				<?php $i++; } ?>
			</table>
		</div>

	</body>
	</html>