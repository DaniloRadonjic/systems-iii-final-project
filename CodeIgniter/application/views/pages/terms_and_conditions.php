<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<style>
		* {
			box-sizing: border-box;
			font-family: 'Open Sans', sans-serif;
		}

		h1 {
			font-family: Montserrat, sans-serif;
			color: #41436A;
			padding: 20px;
			font-size: 36px;
		}

		dl {
			margin: 20px 280px;
			padding: 20px;
		}

		dt {
			color: #41436A;
			font-size: 1.50em;
			font-weight: bold;
			padding: 10px;
		}

		dd {
			margin-bottom: 16px;
		}

	</style>
</head>
<body>

	<h1>Terms & Conditions</h1>
	<dl>
		<dt>1. Introduction</dt>
		<dd>Welcome to 'Delicious Food, Discounted Price', a final project for 2<sup>nd</sup> year 'Systems III - Information Systems' course. This information system is developed as a website, and has a goal of connecting user (<b>'Customer'</b>) with local bakeries, pastry shops and cafes (<b>'Store'</b>) that would like to offer their unsold products at discounted price. Customer browses through the list of posts (offers) that have been made my Stores. Once a Customer finds a product he/she would like to buy, customer then reserves that product, for which he/she receives a product code. Customer goes to the Store and, before purchasing the product, shows a code to the Store, as a proof of a reservation. After Store confirms the reservation (by inserting a code which is provided by customer in a system), it then sells the reserved product to the Customer. Information system purpose and services are available to country of Slovenia. The rest of terms & conditions are dummy text placeholders.</dd><br />
		<dt>2. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel.</dd><br />
		<dt>3. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel.</dd><br />
		<dt>4. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel. In bibendum fringilla metus eget pharetra.</dd><br />
		<dt>5. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel.</dd><br />
		<dt>6. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel.</dd><br />
		<dt>7. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel. In bibendum fringilla metus eget pharetra.</dd><br />
		<dt>8. Lorem ipsum</dt>
		<dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris consectetur ut urna at rhoncus. Donec porta viverra sapien, ac viverra urna sagittis ut. Mauris ultrices volutpat ipsum, vel fermentum ligula ullamcorper egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed ut turpis ac ligula facilisis accumsan. Suspendisse ac dapibus nulla. Phasellus iaculis dapibus ante ut finibus. Maecenas dui nisi, pretium venenatis tortor sodales, sollicitudin egestas purus. Quisque fringilla vitae nunc eu porttitor. In hac habitasse platea dictumst. Etiam non auctor orci. Duis auctor sit amet quam vel tristique. Donec malesuada luctus nunc, a pellentesque mi. Suspendisse hendrerit tempor leo, non congue erat dictum vel. In bibendum fringilla metus eget pharetra.</dd><br />
	</dl>

</body>
</html>