<?php 
if (!isset($this->session->userdata['s_signed_in'])) {
	redirect('restricted');
} 
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<link rel = 'stylesheet' href = '<?php echo base_url();?>styles/my_profile.css' />
</head>
<body>

	<div class = 'navigation_bar'>
		<ul>
			<li class = 'active'>My profile</li>
			<a href = "<?php echo site_url('store_create_post'); ?>"><li>Create post</li></a>
			<a href = "<?php echo base_url('Store/displayposts'); ?>"><li>My posts</li></a>
			<a href = "<?php echo base_url('Store/displayinvoices'); ?>"><li>My invoices</li></a>
			<a href = "<?php echo base_url('Store/signout'); ?>"><li>Sign out</li></a>
		</ul>		
	</div>

	<div class = 'heading'>
		<h1>My profile</h1>
	</div>
	
	<div class = 'main'>
		<form method = 'POST' action = "<?php echo base_url('Store/updateprofile'); ?>">
			<table>
				<tr>
					<td>Email address:</td>
					<td class = 'edit'><?php echo $this->session->userdata['s_signed_in']['email'] ?></td>
					<td><input type = 'email' name = 'new_email' placeholder = 'new email address' required /></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td class = 'edit'><?php echo $this->session->userdata['s_signed_in']['password'] ?></td>
					<td><input type = 'password' name = 'new_password' placeholder = 'new password' required /></td>
				</tr>
				<tr>
					<td>City:</td>
					<td class = 'edit'><?php echo $this->session->userdata['s_signed_in']['city']?></td>
					<td><select name = 'new_city' required>
						<option value = '' disabled selected>select</option>
						<option value = 'Ljubljana'>Ljubljana</option>
						<option value = 'Maribor'>Maribor</option>
						<option value = 'Celje'>Celje</option>
						<option value = 'Kranj'>Kranj</option>
						<option value = 'Koper'>Koper</option>
					</select></td>
				</tr>
				<tr>
					<td>Name of store:</td>
					<td class = 'edit'><?php echo $this->session->userdata['s_signed_in']['name'] ?></td>
					<td><input type = 'text' name = 'new_name' placeholder = 'new name of store'required /></td>
				</tr>
				<tr>
					<td>Address</td>
					<td class = 'edit'><?php echo $this->session->userdata['s_signed_in']['street'] ?></td>
					<td><input type = 'text' name = 'new_address' placeholder = 'new address' required /></td>
				</tr>
			</table>
		</div>
		<div class = 'section'>
			<button class = 'button_edit'>Edit profile</button>
			<input type = 'submit' name = 'update_profile' value = 'Update profile' onclick = 'return confirm_action()' />
			<a onclick = 'return confirm_action()' href = "<?php echo base_url('Store/deleteprofile'); ?>"><button type = 'button' class = 'button_delete'>Delete profile</button></a>
		</div>
	</form>
	
</body>
<script src = '<?php echo base_url();?>script/my_profile.js'>
</script>
</html>