<?php 
class Customer_model extends CI_Model {

	public function save_record($data) {
		$query_insert = "INSERT INTO Customer (email, password, city) VALUES ('" . $data['email'] . "', '" . $data['password'] . "', '" . $data['city'] . "');";
		return $this->db->query($query_insert);
	}

	public function get_id($data) {
		$this->db->select('id')->from('Customer')->where('email', $data['email']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		}
	}

	public function check_if_exist($data) {
		$condition = "email = " . "'" . $data['email'] . "'";
		$this->db->select('*');
		$this->db->from('Customer');
		$this->db->where($condition);
		$this->db->limit(1);
		$query_customer = $this->db->get();
		$this->db->select('*');
		$this->db->from('Store');
		$this->db->where($condition);
		$this->db->limit(1);
		$query_store = $this->db->get();

		if ($query_customer->num_rows() > 0 || $query_store->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function show_profile($email) {
		$condition = "email = " . "'" . $email . "'";
		$this->db->select('*');
		$this->db->from('Customer');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function update_profile($data) {
		$this->db->set($data);
		$this->db->where('id', $data['id']);
		$this->db->update('Customer');
	}

	public function delete_profile($id) {
		$this->db->where('id', $id);
		$this->db->delete('Customer');
	}

	public function get_posts($city) {
		$condition = ("created_at > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY)");
		$this->db->select('
			Product.id AS p_id,
			Product.name AS p_name,
			Product.price AS p_price,
			Product.quantity AS p_quantity,
			Product.code AS p_code,
			Product.created_at AS p_created_at,
			Product.pick_until AS p_pick_until,
			Product.is_reserved AS p_is_reserved,
			Product.is_purchased AS p_is_purchased,
			Product.store_id AS p_store_id,
			Store.id AS s_id,
			Store.name AS s_name,
			Store.address AS s_address,
			Store.city AS s_city'
		);
		$this->db->from('Product');
		$this->db->from('Store');
		$this->db->where('Product.store_id = Store.id');
		$this->db->where($condition);
		$this->db->where('is_reserved', '0');
		$this->db->where('is_purchased', '0');
		$this->db->where('city', $city);
		$query = $this->db->get();
		return $query;
	}

	public function reserve_post($c_id, $p_id) {
		$data = array(
			'reserved_at' => date('Y-m-d H:i:sa'),
			'customer_id' => $c_id,
			'product_id' => $p_id
		);
		$this->db->insert('Reservation', $data);
		$this->db->where('id', $p_id);
		$this->db->set('is_reserved', '1');
		$this->db->update('Product');
	}

	public function get_reservations($id) {
		$condition = ("created_at > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY)");
		$this->db->select('
			Product.id AS p_id,
			Product.name AS p_name,
			Product.price AS p_price,
			Product.quantity AS p_quantity,
			Product.code AS p_code,
			Product.created_at AS p_created_at,
			Product.pick_until AS p_pick_until,
			Product.is_reserved AS p_is_reserved,
			Product.is_purchased AS p_is_purchased,
			Product.store_id AS p_store_id,
			Store.id AS s_id,
			Store.name AS s_name,
			Store.address AS s_address,
			Store.city AS s_city,
			Reservation.reserved_at AS r_reserved_at'
		);
		$this->db->from('Product');
		$this->db->from('Store');
		$this->db->from('Reservation');
		$this->db->where('Reservation.customer_id = ' . $id);
		$this->db->where('Reservation.product_id = Product.id');
		$this->db->where('Product.store_id = Store.id');
		$this->db->where($condition);
		$this->db->where('is_reserved', '1');
		$this->db->where('is_purchased', '0');
		$query = $this->db->get();
		return $query;
	}

	public function get_invoices($id) {
		$this->db->select('
			Product.id AS p_id,
			Product.name AS p_name,
			Product.price AS p_price,
			Product.quantity AS p_quantity,
			Product.code AS p_code,
			Product.created_at AS p_created_at,
			Product.pick_until AS p_pick_until,
			Product.is_reserved AS p_is_reserved,
			Product.is_purchased AS p_is_purchased,
			Product.store_id AS p_store_id,
			Store.id AS s_id,
			Store.name AS s_name,
			Store.address AS s_address,
			Invoice.issued_at AS i_issued_at'
		);
		$this->db->from('Product');
		$this->db->from('Store');
		$this->db->from('Reservation');
		$this->db->from('Invoice');
		$this->db->where('Reservation.product_id = Product.id');
		$this->db->where('Product.store_id = Store.id');
		$this->db->where('Invoice.customer_id = ' . $id);
		$this->db->where('Invoice.store_id = Store.id');
		$this->db->where('Invoice.product_id = Product.id');
		$this->db->where('is_reserved', '1');
		$this->db->where('is_purchased', '1');
		$query = $this->db->get();
		return $query;
	}

}
