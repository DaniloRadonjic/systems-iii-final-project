<?php 
class Store_model extends CI_Model {

	public function save_record($data) {
		$query_insert = "INSERT INTO Store (email, password, city, name, address) VALUES ('" . $data['email'] . "', '" . $data['password'] . "', '" . $data['city'] . "', '" . $data['name'] . "', '" . $data['address'] . "');";
		return $this->db->query($query_insert);
	}

	public function get_id($data) {
		$this->db->select('id')->from('Store')->where('email', $data['email']);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		}
	}

	public function check_if_exist($data) {
		$condition = "email = " . "'" . $data['email'] . "'";
			$this->db->select('*');
			$this->db->from('Customer');
			$this->db->where($condition);
			$this->db->limit(1);
			$query_customer = $this->db->get();
			$this->db->select('*');
			$this->db->from('Store');
			$this->db->where($condition);
			$this->db->limit(1);
			$query_store = $this->db->get();

			if ($query_customer->num_rows() > 0 || $query_store->num_rows() > 0) {
				return TRUE;
			} else {
				return FALSE;
			}
	}

	public function show_profile($email) {
		$condition = "email = " . "'" . $email . "'";
		$this->db->select('*');
		$this->db->from('Store');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return FALSE;
		}
	}

	public function update_profile($data) {
		$this->db->set($data);
		$this->db->where('id', $data['id']);
		$this->db->update('Store');
	}

	public function delete_profile($id) {
		$this->db->where('id', $id);
		$this->db->delete('Store');
	}

	public function save_post($data) {
		$this->db->insert('Product', $data);
	}

	public function get_active_posts($id) {
		$date = date('Y-m-d H:i:sa');
		$condition = ("created_at > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY);");
		$array = array(
			'store_id' => $id,
			'is_purchased' => '0'
		);
		$this->db->select('*');
		$this->db->from('Product');
		$this->db->where($array);
		$this->db->where($condition);
		$query = $this->db->get();
		return $query;
	}

	public function check_code($id, $code) {
		$this->db->select('*');
		$this->db->from('Product');
		$this->db->where('id', $id);
		$query = $this->db->get();
		if ($query->row()->code === $code) {
			$this->db->where('id', $id);
			$data = array(
				'is_purchased' => '1'
			);
			$this->db->update('Product', $data);
			return true;
		}
		return false;
	}

	public function issue_invoice($p_id, $s_id) {
		$this->db->select('*');
		$this->db->from('Reservation');
		$this->db->where('Reservation.product_id', $p_id);
		$query = $this->db->get();
		$c_id = $query->row()->customer_id;
		$data = array(
			'issued_at' => date('Y-m-d H:i:sa'),
			'customer_id' => $c_id,
			'product_id' => $p_id,
			'store_id' => $s_id
		);
		$this->db->insert('Invoice', $data);
		$this->db->where('id', $p_id);
		$this->db->set('is_purchased', '1');
		$this->db->update('Product');
	}

	public function get_invoices($id) {
		$this->db->select('
			Product.id AS p_id,
			Product.name AS p_name,
			Product.price AS p_price,
			Product.quantity AS p_quantity,
			Product.code AS p_code,
			Product.created_at AS p_created_at,
			Product.pick_until AS p_pick_until,
			Product.is_reserved AS p_is_reserved,
			Product.is_purchased AS p_is_purchased,
			Product.store_id AS p_store_id,
			Invoice.issued_at AS i_issued_at,
			Invoice.store_id AS i_customer_id,
			Invoice.customer_id AS i_store_id,
			Invoice.product_id AS i_product_id'
		);
		$this->db->from('Product');
		$this->db->from('Reservation');
		$this->db->from('Invoice');
		$this->db->where('Reservation.product_id = Product.id');
		$this->db->where('Product.store_id', $id);
		$this->db->where('Invoice.store_id = ' . $id);
		$this->db->where('Invoice.product_id = Product.id');
		$this->db->where('is_reserved', '1');
		$this->db->where('is_purchased', '1');
		$query = $this->db->get();
		return $query;
	}

}
