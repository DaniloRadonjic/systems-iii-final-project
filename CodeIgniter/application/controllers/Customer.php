<?php 
class Customer extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('Customer_model');
	}

	public function savedata() {
		if ($this->input->post('sign_up')) {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'city' => $this->input->post('city'),
			);
			$query = $this->Customer_model->check_if_exist($data);
			if ($query == FALSE) {
				$this->Customer_model->save_record($data);
				$this->session->set_flashdata('success', 'Account successfully created.');
				redirect('landing_page');
			} else {
				$this->session->set_flashdata('error', 'Account already taken.');
				redirect('customer_sign_up');
			}
		}
	}

	public function updateprofile() {
		$id = $this->session->userdata['c_signed_in']['id'];
		if ($this->input->post('update_profile')) {
			$data = array(
				'id' => $id,
				'email' => $this->input->post('new_email'),
				'password' => $this->input->post('new_password'),
				'city' => $this->input->post('new_city')
			);
			$this->Customer_model->update_profile($data);
			$this->session->unset_userdata('c_signed_in');
			$this->session->set_flashdata('success', 'Account successfully updated.');
			redirect('landing_page');
		}
	} 

	public function deleteprofile() {
		$id = $this->session->userdata['c_signed_in']['id'];
		$this->Customer_model->delete_profile($id);
		$this->session->unset_userdata('c_signed_in');
		$this->session->set_flashdata('success', 'Account successfully deleted.');
		redirect('landing_page');
	} 

	public function signout() {
		$this->session->unset_userdata('c_signed_in');
		$this->session->set_flashdata('success', 'Successfully signed out.');
		redirect('landing_page');
	}

	public function displayposts() {
		$city = $this->session->userdata['c_signed_in']['city'];
		$data['products'] = $this->Customer_model->get_posts($city);
		if (NULL !== $this->session->flashdata('city')) {
			$this->session->unset_flashdata('city');
		}
		$this->load->view('pages/customer_find_food', $data);
	}

	public function filterposts() {
		if ($this->input->get('filter')) {
			$city = $this->input->get('city');
			$data['products'] = $this->Customer_model->get_posts($city);
			$this->session->set_flashdata('city', $city);
			$this->session->keep_flashdata('city');
			$this->load->view('pages/customer_find_food', $data);
		}
	}

	public function reservepost() {
		$c_id = $this->session->userdata['c_signed_in']['id'];
		if ($this->input->post('reserve')) {
			$p_id = $this->input->post('p_id');
			$this->Customer_model->reserve_post($c_id, $p_id);
			$this->session->set_flashdata('success', 'Post reserved successfully.');
			redirect('Customer/displayreservations');
		}
	}

	public function displayreservations() {
		$c_id = $this->session->userdata['c_signed_in']['id'];
		$data['reservations'] = $this->Customer_model->get_reservations($c_id);
		$this->load->view('pages/customer_my_reservations', $data);
	}

	public function displayinvoices() {
		$c_id = $this->session->userdata['c_signed_in']['id'];
		$data['invoices'] = $this->Customer_model->get_invoices($c_id);
		$this->load->view('pages/customer_my_invoices', $data);
	}
	
}
?>