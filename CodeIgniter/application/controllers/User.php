<?php 
class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('Customer_model');
		$this->load->model('Store_model');
	}

	public function signin() {
		if ($this->input->post('sign_in')) {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password')
			);
			$query_customer = $this->db->query("SELECT * FROM Customer WHERE email = '" . $data['email'] . "' AND password = '" . $data['password'] . "'");
			$query_store = $this->db->query("SELECT * FROM Store WHERE email = '" . $data['email'] . "' AND password = '" . $data['password'] . "'");
			$c_number_of_rows = $query_customer->num_rows();
			$s_number_of_rows = $query_store->num_rows();
		}
		
		if ($c_number_of_rows == 1) {
			$email = $this->input->post('email');
			$result = $this->Customer_model->show_profile($email);
			$c_data = array(
				'id' => $result[0]->id,
				'email' => $result[0]->email,
				'password' => $result[0]->password,
				'city' => $result[0]->city
			);
			$this->session->set_userdata('c_signed_in', $c_data);
			redirect('customer_my_profile');
		} else if ($s_number_of_rows == 1) {
				$email = $this->input->post('email');
				$result = $this->Store_model->show_profile($email);
				$s_data = array(
					'id' => $result[0]->id,
					'email' => $result[0]->email,
					'password' => $result[0]->password,
					'city' => $result[0]->city,
					'name' => $result[0]->name,
					'street' => $result[0]->address
				);
				$this->session->set_userdata('s_signed_in', $s_data);
				redirect('store_my_profile');
			} else {
			$this->session->set_flashdata('incorrect', 'Incorrect email address or password.');
			redirect('landing_page');
	}
}
}

?>