<?php 
class Store extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->model('Store_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
	}

	public function savedata() {
		if ($this->input->post('sign_up')) {
			$data = array(
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
				'name' => $this->input->post('name'),
				'city' => $this->input->post('city'),
				'address' => $this->input->post('address')
			);
			$query = $this->Store_model->check_if_exist($data);
			if ($query == FALSE) {
				$this->Store_model->save_record($data);
				$this->session->set_flashdata('success', 'Account successfully created.');
				redirect('landing_page');
			} else {
				$this->session->set_flashdata('error', 'Account already taken.');
				redirect('store_sign_up');
			}
		}
	}

	public function updateprofile() {
		$id = $this->session->userdata['s_signed_in']['id'];
		if ($this->input->post('update_profile')) {
			$data = array(
				'id' => $id,
				'email' => $this->input->post('new_email'),
				'password' => $this->input->post('new_password'),
				'city' => $this->input->post('new_city'),
				'name' => $this->input->post('new_name'),
				'address' => $this->input->post('new_address')
			);
			$this->Store_model->update_profile($data);
			$this->session->unset_userdata('s_signed_in');
			$this->session->set_flashdata('success', 'Account successfully updated.');
			redirect('landing_page');
		}
	} 

	public function deleteprofile() {
		$id = $this->session->userdata['s_signed_in']['id'];
		$this->Store_model->delete_profile($id);
		$this->session->unset_userdata('s_signed_in');
		$this->session->set_flashdata('success', 'Account successfully deleted.');
		redirect('landing_page');
	} 

	public function signout() {
		$this->session->unset_userdata('s_signed_in');
		$this->session->set_flashdata('success', 'Successfully signed out.');
		redirect('landing_page');
	}

	public function publishpost() {
		$id = $this->session->userdata['s_signed_in']['id'];
		if ($this->input->post('create')) {
			$post_data = array(
				'name' => $this->input->post('name'),
				'quantity' => $this->input->post('quantity'),
				'price' => $this->input->post('euro') . '.' . (strlen($this->input->post('cent')) < 2 ? '0' . $this->input->post('cent') : $this->input->post('cent')),
				'pick_until' => (strlen($this->input->post('hour')) < 2 ? '0' . $this->input->post('hour') : $this->input->post('hour')) . ':' . (strlen($this->input->post('minute')) < 2 ? '0' . $this->input->post('minute') : $this->input->post('minute')),
				'created_at' => date('Y-m-d H:i:sa'),
				'is_reserved' => 0,
				'is_purchased' => 0,
				'code' => rand (100000, 999999),
				'store_id' => $id
			);
			$this->Store_model->save_post($post_data);
			$this->session->set_flashdata('success', 'Post created successfully.');
			redirect('Store/displayposts');
		}
	}

	public function displayposts() {
		$id = $this->session->userdata['s_signed_in']['id'];
		$data['products'] = $this->Store_model->get_active_posts($id);
		$this->load->view('pages/store_my_posts', $data);
	}

	public function checkcode() {
		$p_id = $this->input->post('p_id');
		$s_id = $this->session->userdata['s_signed_in']['id'];
		$code = $this->input->post('code');
		$is_matched = $this->Store_model->check_code($p_id, $code);
		if ($is_matched === true) {
			$this->session->set_flashdata('success', 'Code matches with the product.<br>You may sell the product to the customer.');
			$this->Store_model->issue_invoice($p_id, $s_id);
			redirect('Store/displayinvoices');
		} else {
			$this->session->set_flashdata('error', 'Code does not match with the product.');
			redirect('Store/displayposts');
		}
	}

	public function displayinvoices() {
		$s_id = $this->session->userdata['s_signed_in']['id'];
		$data['invoices'] = $this->Store_model->get_invoices($s_id);
		$this->load->view('pages/store_my_invoices', $data);
	}

}
?>