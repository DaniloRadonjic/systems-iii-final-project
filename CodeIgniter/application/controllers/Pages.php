<?php 
class Pages extends CI_Controller {

	public function view($page = 'landing_page')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
                // Whoops, we don't have a page for that!
			show_404();
		}

		$search = array("store", "customer", '_');
		$replace = array('', '', ' ');
		$title = str_replace($search, $replace, $page);
		$data['title'] = ucwords($title);
		$maintenance = TRUE;

		if ($maintenance === TRUE) {
			$this->load->helper('url');
			$this->load->view('pages/maintenance');
		} else {
			$this->load->helper('url');
			$this->load->view('pages/'.$page, $data);
		}
	}
}

?>

